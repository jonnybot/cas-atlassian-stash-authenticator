# Jasig CAS SSO for Atlassian Stash

This self-containing add-on implements single sign-on for Atlassian Stash for UI operations. 
Single sign-on functionality is available only from official Stash URL configured in general configurations.
This allows to login by standard means (login form) accessing by localhost or IP address.

## Installation

Installation is realized by UPM.
For proper functionality you need to set up plugin properties stash-config.properties:

- *plugin.cas-authenticator.base.url* - Base URL of CAS Server (reguired)  
- *plugin.cas-authenticator.logout.url* - Alternate logout URL (if not specified, single logout page URL of CAS is used) 
- *plugin.cas-authenticator.validator.class* - Validator class (defaults to _org.jasig.cas.client.validation.Cas20ServiceTicketValidator_)
- *plugin.cas-authenticator.validator.XXX* - Properties of above validator class (for example _plugin.cas-authenticator.validator.renew=true")

## Implementation notes

Request identified by basic authentication are ignored. Also request from other URLs as official configured Stash URL. 

- authenticate() raises authentication exception (only for /mvc/login)
- failure handler redirects to CAS
-- with service - link back to Stash
-- with j_username for activating redirection to previous location
- authenticate() uses provided ticket to validate and retrieve assertion

