package sk.eea.stash.cas;

import org.jasig.cas.client.util.ReflectUtils;
import org.jasig.cas.client.validation.TicketValidator;
import org.junit.Assert;
import org.junit.Test;

import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;

public class CasSsoServiceTest {

    @Test
    public void testValidatorInspection() throws Exception {
        final Class<TicketValidator> validatorClass = ReflectUtils.loadClass(CasSsoService.DEFAULT_VALIDATOR);
        final TicketValidator validator = ReflectUtils.newInstance(validatorClass, "http://localhost");
        Assert.assertNotNull(validator);

        final BeanInfo info = Introspector.getBeanInfo(validatorClass);
        for (final PropertyDescriptor pd: info.getPropertyDescriptors()) {
            if ("renew".equals(pd.getName())) {
                Assert.assertNotNull(pd.getWriteMethod());
            }
        }
    }
}