package sk.eea.stash.cas;

import com.atlassian.stash.auth.*;
import com.atlassian.stash.i18n.KeyedMessage;
import com.atlassian.stash.user.*;
import org.jasig.cas.client.validation.Assertion;
import org.jasig.cas.client.validation.TicketValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.ws.rs.core.UriBuilder;
import java.io.IOException;

import static com.atlassian.stash.auth.HttpAuthenticationContext.METHOD_BASIC;

/**
 * Implementation of Jasig CAS authentication handler.
 *
 */
public class CasAuthenticationHandler implements HttpAuthenticationHandler,
        HttpAuthenticationFailureHandler, HttpLogoutHandler {
    private static final Logger LOG = LoggerFactory.getLogger(CasAuthenticationHandler.class);
    private static final String COOKIE_TOKEN_KEY = "_const_cas_assertion_";
    private static final String SESSION_TOKEN_KEY = COOKIE_TOKEN_KEY;
    public static final String CAS_TICKET_MISSING = "cas.ticket.missing";

    private final CasSsoService casSsoService;
    private final UserService userService;

    public CasAuthenticationHandler(UserService userService, CasSsoService casSsoService) {
        this.userService = userService;
        this.casSsoService = casSsoService;
    }

    @Override
    public void validateAuthentication(@Nonnull HttpAuthenticationContext httpAuthenticationContext) {
        final HttpServletRequest request = httpAuthenticationContext.getRequest();
        String token = getToken(request);
        if (token == null) return;

        // TODO check assertion - at the moment left on Stash session
        LOG.info("CAS should validate session periodically...");
    }

    @Nullable
    @Override
    public StashUser authenticate(@Nonnull HttpAuthenticationContext context) {
        final HttpServletRequest request = context.getRequest();

        if (METHOD_BASIC.equals(context.getMethod())) {
            LOG.debug("Skipping CAS SSO as the request is using BASIC authentication");
            return null;
        }
        if (!casSsoService.isSsoCandidate(request)) {
            return null;
        }
        LOG.debug("CAS Authenticate: " + context.getRequest().getRequestURI());

        StashUser user = null;
        String ticket = request.getParameter("ticket");
        if (ticket == null) {
            throw new IncorrectPasswordAuthenticationException(casTicketNotProvided());
        } else {
            String next = request.getParameter("next");
            String service = casSsoService.getServiceURL(next);
            try {
                TicketValidator ticketValidator = casSsoService.getTicketValidator();
                LOG.debug("Attempting ticket validation with service={}  and ticket={}", service, ticket);
                Assertion assertion = ticketValidator.validate(ticket, service);

                final String username = assertion.getPrincipal().getName();
                LOG.info("Authenticated CAS username: " + username);

                if (username != null)
                    user = userService.getUserByName(username);
                if (user != null) {
                    LOG.debug("User " + username + " exists in Stash... continue.");
                    setToken(request, username);
                }
            } catch (final Exception e) {
                LOG.info("Login failed due to CAS ticket validation failure: {}", e);
            }
        }
        return user;
    }

    private KeyedMessage casTicketNotProvided() {
        return new KeyedMessage(CAS_TICKET_MISSING, "No Ticket for CAS found. Redirect to CAS login page...", "No CAS ticket!");
    }

    @Override
    public boolean onAuthenticationFailure(@Nonnull HttpAuthenticationFailureContext context) throws ServletException, IOException {
        if (casSsoService.isSsoCandidate(context.getRequest())) {
            // check reason to avoid loop

            @SuppressWarnings("ThrowableResultOfMethodCallIgnored")
            final String key = context.getAuthenticationException().getMessageKey();
            if (CAS_TICKET_MISSING.equals(key)) {
                try {
                    String next = context.getRequest().getParameter("next");
                    UriBuilder uri = UriBuilder.fromUri(casSsoService.getCasLoginURL()).queryParam("service", casSsoService.getServiceURL(next));
                    String url = uri.build().toString();
                    LOG.debug("Redirecting to: " + url);
                    context.getResponse().sendRedirect(url);
                    return true;
                } catch (IllegalArgumentException e) {
                    LOG.error("CAS base url is incorrect?", e);
                }
            }
        }
        return false;
    }


    @Override
    public void logout(@Nonnull StashUser stashUser, @Nonnull HttpServletRequest httpServletRequest, @Nonnull HttpServletResponse httpServletResponse) {
        LOG.debug("LOGOUT: " + stashUser);
        try {
            // only if authenticated by token
            if (getToken(httpServletRequest) != null) {
                removeToken(httpServletRequest);
                httpServletResponse.sendRedirect(casSsoService.getCasLogoutURL());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void setToken(HttpServletRequest request, String token) {
        HttpSession session = request.getSession();
        session.setAttribute(SESSION_TOKEN_KEY, token);
    }

    private String getToken(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        return session != null ? (String) session.getAttribute(SESSION_TOKEN_KEY) : null;
    }

    private void removeToken(HttpServletRequest request) {
        HttpSession session = request.getSession();
        session.removeAttribute(SESSION_TOKEN_KEY);
    }
}
