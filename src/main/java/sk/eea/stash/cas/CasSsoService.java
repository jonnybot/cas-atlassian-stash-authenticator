package sk.eea.stash.cas;

import com.atlassian.stash.server.ApplicationPropertiesService;
import org.jasig.cas.client.util.ReflectUtils;
import org.jasig.cas.client.validation.TicketValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.UriBuilder;
import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.net.URI;
import java.net.URISyntaxException;

/**
 * Loads and provides CAS configuration form <STASH_HOME>/conf/cas.properties file.
 */
public class CasSsoService {
    public static final String CAS_BASE_URL_PROPERTY = "plugin.cas-authenticator.base.url";
    public static final String CAS_LOGOUT_URL_PROPERTY = "plugin.cas-authenticator.logout.url";
    public static final String CAS_VALIDATOR_PREFIX = "plugin.cas-authenticator.validator.";

    public static final String STASH_LOGIN = "/mvc/login";
    public static final String CAS_SERVICE_CHECK = "/j_cas_service";
    public static final String DEFAULT_VALIDATOR = "org.jasig.cas.client.validation.Cas20ServiceTicketValidator";

    private static final Logger LOG = LoggerFactory.getLogger(CasSsoService.class);

    private final ApplicationPropertiesService applicationPropertiesService;
    private URI casBase = null;
    private TicketValidator ticketValidator;
    private boolean initialized = false;

    public CasSsoService(ApplicationPropertiesService applicationPropertiesService) {
        this.applicationPropertiesService = applicationPropertiesService;
        loadProperties();
    }

    /**
     * Filters only fully qualified HTTP requests.
     * This allows to skip this authentication from localhost (or IP).
     * @param request provided request
     * @return true SSO is applicable
     */
    public boolean isSsoCandidate(HttpServletRequest request) {
        if (isInitialized()) {
            // this is required service
            if (CAS_SERVICE_CHECK.equals(request.getPathInfo())) return true;

            String required = this.applicationPropertiesService.getBaseUrl().toString() + STASH_LOGIN;
            String provided = request.getRequestURL().toString();
            // TODO comparing is rather lame
            LOG.debug("isSsoCandidate: comparing: " + provided + " to " + required);
            return provided.toLowerCase().startsWith(required.toLowerCase());
        } else return false;
    }

    private void loadProperties() {
        String casBaseValue = applicationPropertiesService.getPluginProperty(CAS_BASE_URL_PROPERTY);
        if (casBaseValue != null) {
            try {
                casBase = new URI(casBaseValue);
                ticketValidator = createTicketValidator();
                initialized = true;
                LOG.info("Using CAS: "+casBase);
            } catch (Exception e) {
                LOG.error("CAS not initialized from " + casBase, e);
            }
        }
        else LOG.info("CAS not configured");
    }

    public boolean isInitialized() {
        return initialized;
    }

    public TicketValidator getTicketValidator() {
        return ticketValidator;
    }

    public String getCasLoginURL() {
        return UriBuilder.fromUri(casBase).path("/login").build().toASCIIString();
    }

    /**
     * Provides logout URL redirecting after application logout.
     *
     * @return url from property plugin.cas-authenticator.logout.url, otherwise computed from base.url
     */
    public String getCasLogoutURL() {
        try {
            String logoutUrl = applicationPropertiesService.getPluginProperty(CAS_LOGOUT_URL_PROPERTY);
            if (logoutUrl != null) return new URI(logoutUrl).toASCIIString();
        } catch (URISyntaxException e) {
            LOG.warn(CAS_LOGOUT_URL_PROPERTY + ": parsing exception", e);
        }
        return UriBuilder.fromUri(casBase).path("/logout").build().toASCIIString();
    }

    /**
     * @return service URL send to CAS, where CAS redirects back after successful authentication.
     */
    public String getServiceURL(String next) {
        UriBuilder builder = UriBuilder.fromUri(applicationPropertiesService.getBaseUrl()).path(CAS_SERVICE_CHECK);
        // for redirecting after successful login should be request identified as FORM
        // see StashAuthenticationFilter
        builder.queryParam("j_username", "");
        if (next != null) builder.queryParam("next", next);
        return builder.build().toString();
    }

    /**
     * Creates a {@link TicketValidator} instance from a class name and parameters loaded from plugin properties.
     * @return Ticket validator with properties set.
     */
    private TicketValidator createTicketValidator() {
        String className = applicationPropertiesService.getPluginProperty(CAS_VALIDATOR_PREFIX+"class", "org.jasig.cas.client.validation.Cas20ServiceTicketValidator");

        final Class<TicketValidator> validatorClass = ReflectUtils.loadClass(className);
        final TicketValidator validator = ReflectUtils.newInstance(validatorClass, casBase.toASCIIString());

        try {
            final BeanInfo info = Introspector.getBeanInfo(validatorClass);
            for (final PropertyDescriptor pd: info.getPropertyDescriptors()) {
                if (pd.getWriteMethod() != null) {
                    final String value = applicationPropertiesService.getPluginProperty(CAS_VALIDATOR_PREFIX+pd.getName());
                    if (value != null) {
                        ReflectUtils.setProperty(pd.getName(), convertIfNecessary(pd, value), validator, info);
                        LOG.debug("Set {} = {}", pd.getName(), value);
                    }
                }
            }
        } catch (final IntrospectionException e) {
            throw new RuntimeException("Error getting bean info for " + validatorClass, e);
        }

        return validator;
    }

    /**
     * Attempts to do simple type conversion from a string value to the type expected
     * by the given property.
     *
     * Currently only conversion to int, long, and boolean are supported.
     *
     * @param pd Property descriptor of target property to set.
     * @param value Property value as a string.
     * @return Value converted to type expected by property if a conversion strategy exists.
     */
    private static Object convertIfNecessary(final PropertyDescriptor pd, final String value) {
        if (String.class.equals(pd.getPropertyType())) {
            return value;
        } else if (boolean.class.equals(pd.getPropertyType())) {
            return Boolean.valueOf(value);
        } else if (int.class.equals(pd.getPropertyType())) {
            return new Integer(value);
        } else if (long.class.equals(pd.getPropertyType())) {
            return new Long(value);
        } else {
            throw new IllegalArgumentException(
                    "No conversion strategy exists for property " + pd.getName() + " of type " + pd.getPropertyType());
        }
    }


}
